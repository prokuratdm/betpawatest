package com.example.demo.controller;

import com.example.demo.domain.CurAmount;
import com.example.demo.domain.Customer;
import com.example.demo.domain.Transaction;
import com.example.demo.service.common.WalletService;
import com.example.demo.service.repo.CustomerRepository;
import com.example.demo.src.java.msg.Balance;
import com.example.demo.src.java.msg.Deposit;
import com.example.demo.src.java.msg.Withdraw;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Currency;
import java.util.Optional;

@RestController
public class HelloController {


    @Autowired
    WalletService walletService;

    @RequestMapping("/")
    public String index() {
        return "0-0";
    }

    @RequestMapping("/balance")
    public String balance(@Valid Balance balance) {


        return walletService.getBalance(balance.getId()).toString();
    }

    @RequestMapping("/deposit" )
    @Transactional
    public boolean deposit(@Valid Deposit deposit) {

        return walletService.deposit(deposit);

    }

    @RequestMapping("/withdraw" )
    @Transactional
    public boolean withdraw(@Valid Withdraw withdraw) {

        return walletService.withdraw(withdraw);

    }

}
