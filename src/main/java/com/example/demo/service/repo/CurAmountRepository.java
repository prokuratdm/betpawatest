package com.example.demo.service.repo;

import com.example.demo.domain.CurAmount;
import org.springframework.data.repository.CrudRepository;

public interface CurAmountRepository extends CrudRepository<CurAmount,Long> {
}
