package com.example.demo.service.repo;

import com.example.demo.domain.Customer;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface  CustomerRepository  extends CrudRepository<Customer, Long> {

    /*
    @Query("SELECT c FROM Customer c left join fetch c.amount WHERE c.id = (:id)")
    public Optional<Customer> findByIdAndFetchAmountEagerly(@Param("id") Long id);
*/

}
