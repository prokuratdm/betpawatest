package com.example.demo.service.common;

import com.example.demo.domain.CurAmount;
import com.example.demo.domain.Customer;
import com.example.demo.src.enums.Currency;
import com.example.demo.src.java.msg.Deposit;
import com.example.demo.src.java.msg.Withdraw;
import org.springframework.stereotype.Service;

@Service
public interface CustomerService {
    public Customer createCustomer();
    public Customer getCustomer(Long id);
    public void addTransaction(Customer customer, Deposit deposit);
    public void addTransaction(Customer customer, Withdraw deposit);
    public boolean hasEnoughMoney(Customer customer, Withdraw withdraw);

    public CurAmount getCurAmount(Customer customer, Currency cur);
    public Double getAmountByCur(Customer customer, Currency cur);
    public void changeAmount(Customer customer,Currency cur, Double amount);

}
