package com.example.demo.service.common;

import com.example.demo.domain.CurAmount;
import com.example.demo.domain.Customer;
import com.example.demo.src.enums.Currency;
import com.example.demo.src.exception.RefundException;
import com.example.demo.src.java.msg.Deposit;
import com.example.demo.src.java.msg.Withdraw;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class WalletServiceImpl implements WalletService {


    @Autowired
    CustomerService customerService;

    @Override
    @Transactional
    public boolean deposit(Deposit deposit) {
        Customer customer = customerService.getCustomer(deposit.getId());
        customerService.addTransaction(customer, deposit);
        customerService.changeAmount(customer, deposit.getCurrency(), deposit.getAmount());
        return true;
    }

    @Override
    public boolean canWithdrow(Customer customer, Withdraw withdraw) {
        return customerService.hasEnoughMoney(customer, withdraw);
    }

    @Override
    @Transactional
    public boolean withdraw(Withdraw withdraw) {
        Customer customer = customerService.getCustomer(withdraw.getId());
        if (canWithdrow(customer, withdraw)) {
            customerService.addTransaction(customer, withdraw);
            customerService.changeAmount(customer, withdraw.getCurrency(), -withdraw.getAmount());
            return true;
        } else {
            throw new RefundException("no money, no honey");
        }

    }

    @Override
    public List<CurAmount> getBalance(Long id) {
        return customerService.getCustomer(id).getAmount();
    }
}
