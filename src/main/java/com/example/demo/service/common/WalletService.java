package com.example.demo.service.common;

import com.example.demo.domain.CurAmount;
import com.example.demo.domain.Customer;
import com.example.demo.src.java.msg.Deposit;
import com.example.demo.src.java.msg.Withdraw;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface WalletService {
    public boolean deposit(Deposit deposit);
    public boolean canWithdrow( Customer customer,Withdraw withdraw);
    public boolean withdraw(Withdraw withdraw);
    public List<CurAmount> getBalance(Long id);

}
