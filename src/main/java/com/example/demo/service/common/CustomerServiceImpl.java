package com.example.demo.service.common;

import com.example.demo.domain.CurAmount;
import com.example.demo.domain.Customer;
import com.example.demo.domain.Transaction;
import com.example.demo.service.repo.CustomerRepository;
import com.example.demo.src.enums.Currency;
import com.example.demo.src.java.msg.Deposit;
import com.example.demo.src.java.msg.Withdraw;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    CustomerRepository customerRepository;

    @Override
    public Customer createCustomer() {
        Customer customer = new Customer();
        for (Currency cur : Currency.values()) customer.getAmount().add(new CurAmount(cur, 0.0));
        return customerRepository.save(customer);
    }

    @Override
    public Customer getCustomer(Long id) {
        Optional<Customer> customer = customerRepository.findById(id);
        return customer.isEmpty() ? createCustomer() : customer.get();
    }

    @Override
    public void addTransaction(Customer customer, Deposit deposit) {
        customer.addTransaction(
                new Transaction(
                        deposit.getCurrency(),
                        deposit.getAmount()
                )
        );
        customerRepository.save(customer);
    }

    @Override
    public void addTransaction(Customer customer, Withdraw deposit) {
        customer.addTransaction(
                new Transaction(
                        deposit.getCurrency(),
                        -deposit.getAmount()
                )
        );
        customerRepository.save(customer);
    }

    @Override
    public boolean hasEnoughMoney(Customer customer, Withdraw withdraw) {
        return getAmountByCur(customer, withdraw.getCurrency()) >= withdraw.getAmount();

    }

    @Override
    public CurAmount getCurAmount(Customer customer, Currency cur) {
        Optional<CurAmount> amount = customer.getAmount().stream().filter(it -> it.getCur() == cur).findFirst();
        if (amount.isPresent()) {
            return amount.get();

        }
        CurAmount am = new CurAmount(cur, 0.0);
        customer.getAmount().add(am);
        customerRepository.save(customer);
        return am;

    }

    @Override
    public Double getAmountByCur(Customer customer, Currency cur) {
        return getCurAmount(customer, cur).getAmount();
    }

    @Override
    public void changeAmount(Customer customer, Currency cur, Double amount) {

        CurAmount am = getCurAmount(customer, cur);
        am.setAmount(am.getAmount() + amount);
        customerRepository.save(customer);

    }


}
