package com.example.demo.service.grpc;

import com.example.demo.proto.WithdrawProtoGrpc;
import com.example.demo.proto.WithdrawReq;
import com.example.demo.proto.WithdrawRes;
import com.example.demo.service.common.WalletService;
import com.example.demo.src.enums.Currency;
import com.example.demo.src.exception.RefundException;
import com.example.demo.src.java.msg.Withdraw;
import io.grpc.stub.StreamObserver;
import org.lognet.springboot.grpc.GRpcService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ValidationException;

@GRpcService
public class WithdrawGrpc extends WithdrawProtoGrpc.WithdrawProtoImplBase {

    @Autowired
    WalletService walletService;

    @Override
    public void withdraw(WithdrawReq request, StreamObserver<WithdrawRes> responseObserver) {

        final WithdrawRes.Builder res = WithdrawRes.newBuilder();
        final Withdraw withdraw;


        try {
            withdraw = new Withdraw(
                    Long.parseLong(request.getUserId()),
                    request.getAmount(),
                    Currency.valueOf(request.getCur())//TODO make error
            );
            if (walletService.withdraw(withdraw))
                res.setRes(WithdrawRes.Result.OK);
        } catch (RefundException ex) {
            //ex.printStackTrace();
            res.setRes(WithdrawRes.Result.ERROR);
            res.setError(WithdrawRes.Error.INSUFFICIENT_FUNDS);
        } catch (IllegalArgumentException ex) {
            res.setRes(WithdrawRes.Result.ERROR);
            res.setError(WithdrawRes.Error.UNKNOWN_CURRENCY);
        }
        responseObserver.onNext(res.build());
        responseObserver.onCompleted();


    }
}
