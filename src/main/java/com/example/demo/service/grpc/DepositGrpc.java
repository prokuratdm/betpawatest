package com.example.demo.service.grpc;

import com.example.demo.proto.*;

import com.example.demo.service.common.WalletService;
import com.example.demo.src.enums.Currency;
import com.example.demo.src.java.msg.Deposit;
import io.grpc.stub.StreamObserver;
import org.lognet.springboot.grpc.GRpcService;
import org.springframework.beans.factory.annotation.Autowired;


@GRpcService
public class DepositGrpc extends DepositProtoGrpc.DepositProtoImplBase {

    @Autowired
    WalletService walletService;

    @Override
    public void deposit(DepositReq request, StreamObserver<DepositRes> responseObserver) {


        final DepositRes.Builder res = DepositRes.newBuilder();
        try {
            final Deposit dep = new Deposit(
                    Long.parseLong(request.getUserId()),
                    request.getAmount(),
                    Currency.valueOf(request.getCur())
            );
            if (walletService.deposit(dep))
                res.setRes(DepositRes.Result.OK);

        } catch (IllegalArgumentException ex) {

            res.setRes(DepositRes.Result.ERROR);
            res.setError(DepositRes.Error.UNKNOWN_CURRENCY);
        }

        responseObserver.onNext(res.build());
        responseObserver.onCompleted();

    }
}
