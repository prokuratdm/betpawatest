package com.example.demo.service.grpc;


import com.example.demo.proto.BalanceProtoGrpc;
import com.example.demo.proto.BalanceReq;
import com.example.demo.proto.BalanceRes;
import com.example.demo.proto.CurBalance;
import com.example.demo.service.common.WalletService;
import io.grpc.stub.StreamObserver;
import org.lognet.springboot.grpc.GRpcService;
import org.springframework.beans.factory.annotation.Autowired;

@GRpcService
public class BalanceGrpc extends BalanceProtoGrpc.BalanceProtoImplBase {

    @Autowired
    WalletService walletService;

    @Override
    public void balance(BalanceReq request, StreamObserver<BalanceRes> responseObserver) {


        final long userId = Long.parseLong(request.getUserId());
        var balance = walletService.getBalance(userId);
        final BalanceRes.Builder response = BalanceRes.newBuilder();
        balance.stream().forEach(curBlance ->
                response.addBalance(CurBalance.newBuilder()
                        .setAmount(curBlance.getAmount())
                        .setCurrence(curBlance.getCur().name())
                        .build()));
        responseObserver.onNext(response.build());
        responseObserver.onCompleted();


    }
}
