package com.example.demo.domain;

import org.hibernate.annotations.IndexColumn;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;


    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    @JoinColumn(name = "customer_id")
    private List<CurAmount> amount= new ArrayList<>();



    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    @JoinColumn(name = "customer_id")
    @OrderColumn(name = "idx")
    @LazyCollection(LazyCollectionOption.EXTRA)
    private List<Transaction> transactionList= new ArrayList<>();;


    public Customer() {
    }


    public List<CurAmount> getAmount() {
        return amount;
    }

    private List<Transaction> getTransactionList() {
        return transactionList;
    }

    public void setAmount(List<CurAmount> amount) {
        this.amount = amount;
    }

    public void setTransactionList(List<Transaction> transactionList) {
        this.transactionList = transactionList;
    }

    public void addTransaction(Transaction transaction)
    {
        //this.getTransactionList().add(transaction);
    }

    @Override
    public String toString() {
        return String.format(
                "Customer[id=%d]",
                id);
    }


}
