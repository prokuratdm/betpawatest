package com.example.demo.domain;

import com.example.demo.src.enums.Currency;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class CurAmount {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Currency cur;
    private Double amount;


    public CurAmount() {
    }

    public CurAmount(Currency cur, Double amount) {
        this.cur = cur;
        this.amount = amount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Currency getCur() {
        return cur;
    }

    public void setCur(Currency cur) {
        this.cur = cur;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "CurAmount{" +
                "cur=" + cur +
                ", amount=" + amount +
                '}';
    }
}
