package com.example.demo.src.enums;

public enum Currency {
    EUR,
    USD,
    GBP
}
