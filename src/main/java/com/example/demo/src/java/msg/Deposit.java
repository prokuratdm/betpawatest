package com.example.demo.src.java.msg;

import com.example.demo.src.enums.Currency;

import javax.validation.constraints.Min;
import java.io.Serializable;

public class Deposit implements Serializable {

    private Long id;

    @Min(0)
    private Double amount;


    private Currency currency;

    public Deposit(Long id, Double amount, Currency currency) {
        this.id = id;
        this.amount = amount;
        this.currency = currency;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

}
