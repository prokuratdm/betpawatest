package com.example.demo.src.java.msg;

import com.example.demo.src.enums.Currency;

import java.io.Serializable;

public class Balance implements Serializable {
    private Long id;


    public Balance(Long id) {
        this.id = id;

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


}
