package com.example.demo;

import com.example.demo.domain.Customer;
import com.example.demo.service.common.CustomerService;
import com.example.demo.service.common.WalletService;
import com.example.demo.src.enums.Currency;
import com.example.demo.src.java.msg.Deposit;
import com.example.demo.src.java.msg.Withdraw;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.AssertTrue;

@RunWith(SpringRunner.class)

@SpringBootTest
public class BetPawaApplicationTests {

    @Autowired
    private WalletService walletService;

    @Autowired
    private CustomerService customerService;

    @Test
    @Transactional
    public void simpleTest() {
        Customer customer=customerService.getCustomer(1L);
        //1 step
        RuntimeException ex =Assertions.assertThrows( RuntimeException.class,()->{
                walletService.withdraw(new Withdraw(1L,200d, Currency.USD));});
        Assert.assertTrue(ex.getMessage().equals("no money, no honey"));
        //2 step
        walletService.deposit(new Deposit(1L,100d,Currency.USD));
        //3 step
        Assert.assertTrue(checkBalance(customer, 100,0,0));
        //4 step
        ex =Assertions.assertThrows( RuntimeException.class,()->{
            walletService.withdraw(new Withdraw(1L,200d, Currency.USD));});
        Assert.assertTrue(ex.getMessage().equals("no money, no honey"));
        //5 step
        walletService.deposit(new Deposit(1L,100d,Currency.EUR));
        //6 step
        Assert.assertTrue(checkBalance(customer, 100,100,0));
        //7 step
        ex =Assertions.assertThrows( RuntimeException.class,()->{
            walletService.withdraw(new Withdraw(1L,200d, Currency.USD));});
        Assert.assertTrue(ex.getMessage().equals("no money, no honey"));
        //8 step
        walletService.deposit(new Deposit(1L,100d,Currency.USD));
        //9 step
        Assert.assertTrue(checkBalance(customer, 200,100,0));
        //10 step
        Assert.assertTrue(walletService.withdraw(new Withdraw(1L,200d, Currency.USD)));
        //11 step
        Assert.assertTrue(checkBalance(customer, 0,100,0));
        //12 step
        ex =Assertions.assertThrows( RuntimeException.class,()->{
            walletService.withdraw(new Withdraw(1L,200d, Currency.USD));});
        Assert.assertTrue(ex.getMessage().equals("no money, no honey"));
    }

    private boolean checkBalance(Customer customer, double usd,double eur, double gbp)
    {
        return customerService.getAmountByCur(customer,Currency.USD)==usd&&
                customerService.getAmountByCur(customer,Currency.EUR)==eur&&
                customerService.getAmountByCur(customer,Currency.GBP)==gbp;
    }


}
